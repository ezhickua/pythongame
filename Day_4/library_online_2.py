import datetime
from typing import List, Optional


class Author:
    """
    Represents an author of a book.

    Attributes:
    - first_name (str): The first name of the author.
    - last_name (str): The last name of the author.
    - year_of_birth (int): The year of birth of the author.
    """

    def __init__(
        self, first_name: [str], last_name: [str], year_of_birth: [int]
    ) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        """
        Returns a string representation of the Author.

        Returns:
        - str: The first_name of the author.
        """
        return f"{self.first_name}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Author object.

        Returns:
        - str: A string representation of the Author.
        """
        return f"Author({self.first_name}, {self.last_name}, {self.year_of_birth})"

    def __eq__(self, other: "Author") -> bool:
        """
        Compares two Author objects for equality.

        Args:
        - other (Author): Another Author object.

        Returns:
        - bool: True if the authors are equal, TypeError oweride.
        """
        if not isinstance(other, Author):
            raise TypeError(
                f"for type Author and type {type(other)} operation is not implemented"
            )
        return (
            self.first_name == other.first_name
            and self.last_name == other.last_name
            and self.year_of_birth == other.year_of_birth
        )

    def __hash__(self):
        return (self.first_name, self.last_name, self.year_of_birth)


class Genre:
    """
    Represents a genre of a book.

    Attributes:
    - name (str): The name of the genre.
    - description (str): A description of the genre.
    """

    # def __init__(self, name: str, description: Optional[str] = None) -> None: # Опциональный параметр
    def __init__(self, name: str, description: str) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        Returns a string representation of the Genre.

        Returns:
        - str: The name of the genre.
        """
        return f"{self.name}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Genre object.

        Returns:
        - str: A string representation of the Genre.
        """
        return f"Genre({self.name}, {self.description})"


class Book(Author, Genre):
    """
    Represents a book.

    Attributes:
    - title (str): The title of the book.
    - description (str): A description of the book.
    - language (str): The language of the book.
    - authors (List[Author]): A list of Author objects.
    - genres (List[Genre]): A list of Genre objects.
    - publication_year (int): The year of publication of the book.
    - isbn (str): The ISBN (International Standard Book Number) of the book.
    """

    def __init__(
        self,
        title: str,
        language: str,
        publication_year: int,
        *authors: Author,
        description: Optional[str] = None,
        genres: Optional[List[Genre]] = None,
        isbn: Optional[int] = None,
    ) -> None:
        self.title = title
        self.description = description
        self.language = language
        self.authors = list(authors)
        self.genres = genres if genres is not None else []
        self.publication_year = publication_year
        self.isbn = isbn

    def __str__(self) -> str:
        author_name = ", ".join([author for author in self.authors])
        genre_name = ", ".join([genre.name for genre in self.genres])
        return f"The book is {self.title} \n about: {self.description} \n language:  {self.language} \n author:  {author_name} \n genres:   {genre_name} \n year publication: {self.publication_year} \n ISBN: {self.isbn}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Book object.

        Returns:
        - str: A string representation of the Book.
        """
        return f"{self.title} \n {self.description} \n {self.language} \n {self.authors} \n {self.genres} \n {self.publication_year} \n  {self.isbn}"

    def __eq__(self, other: "Book") -> bool:
        """
        Compares two Book objects for equality.

        Args:
        - other (Book): Another Book object.

        Returns:
        - bool: True if the books are equal, TypeError otherwise.
        """
        if not isinstance(other, Book):
            raise TypeError(
                f"for type Book and type {type(other)} operation is not implemented"
            )
        return self.title == other.title and set(self.authors) == set(other.authors)

    @property
    def book_age(self):
        """The function returns the age of the book relative to the current year"""
        today = datetime.date.today()
        return f" Book age {today.year - int(self.publication_year)} years"


# Приклад використання класів:


genre1 = Genre("Жанр1", "Опис жанру1")
genre2 = Genre("Жанр1", "Опис жанру1")
genre3 = Genre("Жанр2", "Опис жанру2")


book4 = Book(
    "Назва книги4",
    "Мова4",
    2015,
    "Sd",
    "Gtnhjd",
    description="ISBN1234567890",
    genres=[genre3, genre1],
    isbn="ISBN1234567890",
)


print(book4)
print("#" * 25)
print(book4.book_age)

# noinspection PyPackageRequirements
print(repr(book4))
