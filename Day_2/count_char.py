def char_counter(st: str) -> dict[str:float]:
    """Функцію яка приймає рядок і повертає словник у якому ключами є всі символи,
    які зустрічаються в цьому рядку, а значення - вірогідності зустріти цей символ в цьому рядку.
    :param st:
    :return:
    """
    data = {}
    for char in st:
        if char not in data:
            data[char] = round(st.count(char) / len(st) * 100, 2)

    return data


if __name__ == "__main__":
    text = input("\nВведіть Ваш рядок: ")
    result = char_counter(text)
    print(result)
