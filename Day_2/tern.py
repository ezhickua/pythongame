import unittest


def logic(x: int, y: int):
    return (
        x + y
        if x < y
        else (0 if x == y and x != 0 and y != 0 else (x - y if x > y else "game over"))
    )


class TestLogic(unittest.TestCase):
    def test_x_less_than_y(self):
        result = logic(3, 5)
        self.assertEqual(result, 8)

    def test_x_equal_to_y(self):
        result = logic(4, 4)
        self.assertEqual(result, 0)

    def test_x_greater_than_y(self):
        result = logic(8, 5)
        self.assertEqual(result, 3)

    def test_x_and_y_equal_to_zero(self):
        result = logic(0, 0)
        self.assertEqual(result, "game over")


if __name__ == "__main__":
    unittest.main()
