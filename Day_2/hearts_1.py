import random
from typing import List, Tuple, Sequence

from typing_extensions import Any

SUITS = "♠️ ♡ ♢ ♣️".split()
RANKS = list(range(2, 11)) + "J Q K A".split()

Card = Tuple[str, str]
Deck = List[Card]  # list[Card]


def get_deck(shuffle: bool = False) -> List[Tuple[str, str]]:
    """Create new deck 52 cards, if shuffle = True"""
    deck = [(s, r) for r in RANKS for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck


def choose(items: Sequence[Any]) -> Any:
    """Choose and return random item"""
    return random.choice(items)


def player_order(names, start=None):
    """Rotate player order start"""
    if start is None:
        start = choose(names)
    start_idx = names.index(start)
    return names[start_idx:] + names[:start_idx]


def deal_hands(deck: Deck) -> Tuple[Deck, Deck, Deck, Deck]:
    """Deal cards 4 players"""
    return deck[::4], deck[1::4], deck[2::4], deck[3::4]


def play() -> None:
    """4 players play game"""
    deck = get_deck(shuffle=True)
    names = ("PLAYER_ONE PLAYER_TWO PLAYER_THREE PLAYER_FOUR").split()
    hands: dict[str, Deck] = {n: h for n, h in zip(names, deal_hands(deck))}

    for name, cards in hands.items():
        card_str = " ".join(f"{s}{r}" for s, r in cards)
        print(f"{name}: {card_str}")


if __name__ == "__main__":
    play()
