import random
from typing import Tuple, List, Sequence, TypeVar, Optional

SUITS = "♠️ ♡ ♢ ♣️".split()
RANKS = list(range(2, 11)) + "J Q K A".split()

Card = Tuple[str, str]  # tuple[str, str]
Deck = List[Card]  # list[Card]


Choosable = TypeVar("Choosable")


def get_deck(shuffle: bool = False) -> Deck:
    """Create new deck 52 cards, if shuffle = True"""
    deck = [(s, r) for r in RANKS for s in SUITS]
    if shuffle:
        random.shuffle(deck)
    return deck


def choose(items: Sequence[Choosable]) -> Choosable:
    """Choose and return a random item."""
    return random.choice(items)


def player_order(names: List[str], start: Optional[str] = None) -> List[str]:
    """Rotate player order so that start goes first."""
    if start is None:
        start = choose(names)
    start_idx = names.index(start)
    return names[start_idx:] + names[:start_idx]


def deal_hands(deck: Deck) -> Tuple[Deck, Deck, Deck, Deck]:
    """Deal the cards in the deck into four hands"""
    return deck[0::4], deck[1::4], deck[2::4], deck[3::4]


def play() -> None:
    """Play a 4-player card game"""
    deck = get_deck(shuffle=True)
    names = ("PLAYER_ONE PLAYER_TWO PLAYER_THREE PLAYER_FOUR").split()
    hands: dict[str, Deck] = {n: h for n, h in zip(names, deal_hands(deck))}
    start_player = choose(names)
    turn_order = player_order(names, start=start_player)

    while hands[start_player]:
        for name in turn_order:
            card = choose(hands[name])
            hands[name].remove(card)
            print(f"{name}: {card[0] + str(card[1])} ", end="")
        print()


if __name__ == "__main__":
    play()
