from typing import List, Optional


class Author:
    """
    Represents an author of a book.

    Attributes:
    - first_name (str): The first name of the author.
    - last_name (str): The last name of the author.
    - year_of_birth (int): The year of birth of the author.
    """

    def __init__(
        self, first_name: [str], last_name: [str], year_of_birth: [int]
    ) -> None:
        self.first_name = first_name
        self.last_name = last_name
        self.year_of_birth = year_of_birth

    def __str__(self) -> str:
        """
        Returns a string representation of the Author.

        Returns:
        - str: The first_name of the author.
        """
        return f"{self.first_name}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Author object.

        Returns:
        - str: A string representation of the Author.
        """
        return f"Author({self.first_name}, {self.last_name}, {self.year_of_birth})"

    def __eq__(self, other) -> bool:
        """
        Compares two Author objects for equality.

        Args:
        - other (Author): Another Author object.

        Returns:
        - bool: True if the authors are equal, False otherwise.
        """
        if isinstance(other, Author):
            return (
                self.first_name == other.first_name
                and self.last_name == other.last_name
                and self.year_of_birth == other.year_of_birth
            )
        return False

    def __hash__(self):
        return hash((self.first_name, self.last_name, self.year_of_birth))


class Genre:
    """
    Represents a genre of a book.

    Attributes:
    - name (str): The name of the genre.
    - description (str): A description of the genre.
    """

    # def __init__(self, name: str, description: Optional[str] = None) -> None: # Опциональный параметр
    def __init__(self, name: str, description: str) -> None:
        self.name = name
        self.description = description

    def __str__(self) -> str:
        """
        Returns a string representation of the Genre.

        Returns:
        - str: The name of the genre.
        """
        return f"{self.name}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Genre object.

        Returns:
        - str: A string representation of the Genre.
        """
        return f"Genre({self.name}, {self.description})"


class Book(Author, Genre):
    """
    Represents a book.

    Attributes:
    - title (str): The title of the book.
    - description (str): A description of the book.
    - language (str): The language of the book.
    - authors (List[Author]): A list of Author objects.
    - genres (List[Genre]): A list of Genre objects.
    - publication_year (int): The year of publication of the book.
    - isbn (str): The ISBN (International Standard Book Number) of the book.
    """

    def __init__(
        self,
        title: str,
        description: str,
        language: str,
        authors: List[Author],
        genres: List[Genre],
        publication_year: int,
        isbn: int,
    ) -> None:
        self.title = title
        self.description = description
        self.language = language
        self.authors = authors
        self.genres = genres
        self.publication_year = publication_year
        self.isbn = isbn

    def __str__(self) -> str:
        author_name = ", ".join(
            [author.first_name + " " + author.last_name for author in self.authors]
        )
        genre_name = ", ".join([genre.name for genre in self.genres])
        return f"The book is {self.title} \n about: {self.description} \n language:  {self.language} \n author:  {author_name} \n genres:   {genre_name} \n year publication: {self.publication_year} \n ISBN: {self.isbn}"

    def __repr__(self) -> str:
        """
        Returns a string representation that can be used to recreate the Book object.

        Returns:
        - str: A string representation of the Book.
        """
        return f"{self.title} \n {self.description} \n {self.language} \n {self.authors} \n {self.genres} \n {self.publication_year} \n  {self.isbn}"

    '''def __eq__(self, other) -> bool:
        """
        Compares two Book objects for equality.

        Args:
        - other (Book): Another Book object.

        Returns:
        - bool: True if the books are equal, False otherwise.
        """
        if isinstance(other, Book):
            return self.title == other.title and self.authors == other.authors
        return False'''

    def __eq__(self, other: "Book") -> bool:
        """
        Compares two Book objects for equality.

        Args:
        - other (Book): Another Book object.

        Returns:
        - bool: True if the books are equal, False otherwise.
        """
        if isinstance(other, Book):
            return self.title == other.title and set(self.authors) == set(
                other.authors
            )  # set because add __hash__ and autors can be not one
        return False


# Приклад використання класів:

author1 = Author("Ім'я1", "Прізвище1", 1980)
author2 = Author("Ім'я1", "Прізвище1", 1980)
author3 = Author("Ім'я2", "Прізвище2", 1990)


genre1 = Genre("Жанр1", "Опис жанру1")
genre2 = Genre("Жанр1", "Опис жанру1")
genre3 = Genre("Жанр2", "Опис жанру2")


# Створення об'єкта класу Book, який успадковує атрибути і методи класів Author і Genre
book1 = Book(
    "Назва книги1",
    "Опис книги1",
    "Мова1",
    [author1, author3],
    [genre1, genre3],
    2021,
    "ISBN1234567891",
)
book2 = Book(
    "Назва книги1", "Опис книги1", "Мова1", [author2], [genre2], 2022, "ISBN1234567892"
)
book3 = Book(
    "Назва книги3", "Опис книги3", "Мова3", [author3], [genre3], 2020, "ISBN1234567890"
)

print(author1 == author2)
print(author1 == author3)
print(book1 == book2)
print(book1 == book3)
print(str(author1))
print(repr(author3))
print(str(genre2))
print(repr(genre2))
print(book1)
print(book2)
print(repr(book1))
