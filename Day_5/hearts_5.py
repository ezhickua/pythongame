"""
Правила гри, коротко:
Грають четверо гравців, ім роздається французька колода - по 13 карт у кожного.
Гравець, який тримає ♣2, починає перший раунд і повинен починати саме з ♣2.
Гравці по черзі грають свою карту, дотримуючись першої масті, якщо це можливо.
Гравець, який розігрує старшу карту головної масті, збирає "взятку" і стає початковим гравцем у наступному ході.
Гравець не може класти карту з ♡, доки ♡ не буде зіграно в попередньому прийомі (тобто - вперше ♡ може бути розіграна лише якщо у гравця не має масті, з якої почався круг).
Коли всі карти розіграно, гравці отримують очки, якщо вони беруть певні карти:

13 балів за ♠Q
1 бал за кожне ♡

Гра триває кілька раундів, поки один гравець не набере 100 очок або більше. Перемагає гравець з найменшою кількістю балів"""

import random
import sys
from typing import Tuple, List

from colorama import Fore, Back


class Card:
    SUITS = "♠️ ♡ ♢ ♣️".split()
    RANKS = list(range(2, 11)) + "J Q K A".split()

    def __init__(self, suit: str, rank: str) -> None:
        self.suit = suit
        self.rank = rank

    def __repr__(self):
        return f"{Back.LIGHTWHITE_EX}{Fore.RED + self.suit}{Fore.LIGHTBLACK_EX}{self.rank!r:<10}"

    @property
    def value(self) -> int:
        """Value of card is rank number"""
        return self.RANKS.index(self.rank)

    @property
    def points(self) -> int:
        """Points cards"""
        if self.suit == "♠️" and self.rank == "Q":
            return 13
        if self.suit == "♡":
            return 1
        return 0

    def __eq__(self, other: "Card") -> bool:
        if isinstance(other, Card):
            return (self.suit, self.rank) == (other.suit, other.rank)
        raise TypeError(
            f"Unsupported operand type for equality: 'Card' and {type(other)}"
        )

    def __lt__(self, other: "Card") -> bool:
        if isinstance(other, Card):
            # First check suit
            if self.suit != other.suit:
                return self.SUITS.index(self.suit) < self.SUITS.index(other.suit)
            # If suits equal, check ranks
            return self.value < other.value
        raise TypeError(
            f"Unsupported operand type for less than: 'Card' and {type(other)}"
        )


class Deck:
    def __init__(self, cards: List[str]) -> None:
        self.cards = cards

    @classmethod
    def create(cls, shuffle=False):
        cards = [Card(s, r) for r in Card.RANKS for s in Card.SUITS]
        if shuffle:
            random.shuffle(cards)
        return cls(cards)

    def deal(self, num_players: int) -> Tuple:
        """Deal the cards in the deck into some hands"""
        cls = self.__class__
        return tuple(cls(self.cards[i::num_players]) for i in range(num_players))


class Player:
    def __init__(self, name: str, hand: Deck) -> None:
        self.name = name
        self.hand = hand

    def play_card(self) -> Card:
        """Play a card from 1 player hands"""
        card = random.choice(self.hand.cards)
        self.hand.cards.remove(card)
        print(
            f"{Back.LIGHTWHITE_EX}{Fore.LIGHTBLUE_EX + self.name}:{card!r}  {Back.RESET}",
            end="",
        )


class Game:
    def __init__(self, *names: str) -> None:
        deck = Deck.create(shuffle=True)
        self.names = (
            (list(names) + "P1 P2 P3 P4".split())[:4]
            if names
            else "P1 P2 P3 P4".split()[:4]
        )

        self.hands = {n: Player(n, h) for n, h in zip(self.names, deck.deal(4))}

    def player_order(self, start=None):
        if start is None:
            start = random.choice(self.names)
        start_idx = self.names.index(start)
        return self.names[start_idx:] + self.names[:start_idx]

    def play(self) -> None:
        """Play the card game"""
        start_player = random.choice(self.names)
        turn_order = self.player_order(start=start_player)

        while self.hands[start_player].hand.cards:
            for name in turn_order:
                self.hands[name].play_card()
            print()


if __name__ == "__main__":
    player_names = sys.argv[1:]
    game = Game(*player_names)
    game.play()
